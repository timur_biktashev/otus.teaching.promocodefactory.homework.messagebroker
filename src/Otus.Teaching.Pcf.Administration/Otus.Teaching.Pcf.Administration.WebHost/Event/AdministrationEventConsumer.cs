using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.Administration.Core.EventContract;

namespace Otus.Teaching.Pcf.Administration.WebHost.Event
{
    internal class AdministrationEventConsumer :
        IConsumer<IPromoCodeEvent>
    {
        private readonly IEmployeeService employeeService;

        public AdministrationEventConsumer(IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeEvent> context)
        {
            await employeeService.UpdateAppliedPromocodesAsync(context.Message);
        }
    }
}