using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.EventContract
{
    public interface IPromoCodeEvent
    {
        string ServiceInfo { get; }
        Guid PartnerId { get; }
        Guid PromoCodeId { get; }
        string PromoCode { get; }
        Guid PreferenceId { get; }
        string BeginDate { get; }
        string EndDate { get; }
        Guid? PartnerManagerId { get; }
    }
}