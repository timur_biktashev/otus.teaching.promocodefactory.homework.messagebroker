﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.EventContract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public interface IPromoCodesService
    {
        Task<IEnumerable<PromoCode>> GetPromocodesAsync();
        Task<PromoCode> GivePromoCodesToCustomersWithPreferenceAsync(IPromoCodeEvent message);
    }
}