using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.EventContract;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromoCodesService : IPromoCodesService
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        
        public PromoCodesService(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<IEnumerable<PromoCode>> GetPromocodesAsync()
        {
            return await _promoCodeRepository.GetAllAsync();
        }

        public async Task<PromoCode> GivePromoCodesToCustomersWithPreferenceAsync(IPromoCodeEvent message)
        {
            var preference = await _preferenceRepository.GetByIdAsync(message.PreferenceId);
            if (preference == null)
            {
                return null;
            }

            var customers = await _customerRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            var promoCode = MapFromMessage(message, preference, customers);

            await _promoCodeRepository.AddAsync(promoCode);
            
            return promoCode;
        }
        
        private static PromoCode MapFromMessage(IPromoCodeEvent message, Preference preference, IEnumerable<Customer> customers)
        {
            var promocode = new PromoCode
            {
                Id = message.PromoCodeId,
                PartnerId = message.PartnerId,
                Code = message.PromoCode,
                ServiceInfo = message.ServiceInfo,
                BeginDate = DateTime.Parse(message.BeginDate),
                EndDate = DateTime.Parse(message.EndDate),
                Preference = preference,
                PreferenceId = preference.Id,
                Customers = new List<PromoCodeCustomer>()
            };

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {
                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }
    }
}