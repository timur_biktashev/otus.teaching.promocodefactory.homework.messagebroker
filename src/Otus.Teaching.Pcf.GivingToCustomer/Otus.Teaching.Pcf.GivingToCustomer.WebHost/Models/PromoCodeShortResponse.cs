﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class PromoCodeShortResponse
    {
        public PromoCodeShortResponse()
        {

        }
        public PromoCodeShortResponse(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            BeginDate = promoCode.BeginDate.ToString("yyyy-MM-dd");
            EndDate = promoCode.EndDate.ToString("yyyy-MM-dd");
            PartnerId = promoCode.PartnerId;
            ServiceInfo = promoCode.ServiceInfo;
        }

        public Guid Id { get; set; }
        
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public Guid PartnerId { get; set; }
    }
}