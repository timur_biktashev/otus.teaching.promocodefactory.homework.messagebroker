using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.EventContract;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Event
{
    internal class CustomerEventConsumer :
        IConsumer<IPromoCodeEvent>
    {
        private readonly IPromoCodesService promoCodesService;
        
        public CustomerEventConsumer(
            IPromoCodesService promoCodesService)
        {
            this.promoCodesService = promoCodesService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeEvent> context)
        {
            await promoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}